#!/usr/bin/bash
podman run -it --rm \
  --security-opt label=disable \
  -v `pwd`:/antora:z antora/antora  \
	--stacktrace \
	site.yml
#	git commit -a -m "Built on $(date)" && git push origin master
	#--cache-dir=./.cache  \
