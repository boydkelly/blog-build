#!/usr/bin/bash
[[ -x /usr/bin/podman ]] || { echo "Run outside of toolbox!" ; exit 1 ; } 
export AUTH=cSXqNYC54yL3y1CRmgjd
echo $AUTH
sed s/\$AUTH/"$AUTH"/g local.yml > tmp.yml

cp -v ../blog-ui/build/ui-bundle.zip .
podman run -it --rm \
  --security-opt label=disable \
  -v `pwd`:/antora:z antora/antora  \
	--stacktrace \
	--ui-bundle-url=ui-bundle.zip \
	tmp.yml 
	#--cache-dir=./.cache  \
rm tmp.yml
cd public && python -m http.server 8080
#git commit -a -m "Built on $(date)" && git push origin master
	local.yml > antora.log
